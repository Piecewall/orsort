/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orsort;
import java.util.Arrays;
import java.util.Scanner;

public class Orsort {


    public static void main(String[] args) {

        Orsort ordenar = new Orsort();
        //mis arreglos del programa
        String[] Ap = {"Cach", "Ayala", "Gaytan", "Zaragosa", "Pinzon"};
        
        String[] Nom = {"Emiliano", "Orlando", "Rodrigo", "Juan Carlos", "Yazuri"};

        String[] Tel = {"913-123-55-77", "981-453-44-88", "913-432-22-33", "981-567-33-45", "913-233-66-99"};
        System.out.println("Arreglo original");
        ordenar.mostrarArreglo(Ap);
        System.out.println("");
        //Ordena el array
        System.out.println("Arreglo ordenado");
        ordenar.ordenararreglo(Ap);
        
        //solicitarle al usuario que apellido quiere verificar
        Scanner sc = new Scanner(System.in);
        String dato="";
        System.out.println("\nIngrese el apellido a buscar");
        dato = sc.nextLine();
        //aplicamos el metodo de busqueda binaria
        busq_binaria(Ap, dato);
        //creamos una variable auxiliar llamada posicion
        int posicion = busq_binaria(Ap, dato);  
        if(posicion == -1){
           //se compara para que se sepa si se busco el dato o no 
        }else{
            //se imprimen los demas arreglos
            impdat(Ap,Tel,Nom, posicion);
        }
        
    }
    
    public static void ordenararreglo(String[] arreglo){
        Arrays.sort(arreglo);
        //Mostramos el array ya ordenado
        for (String i : arreglo) {
            System.out.print(i + ", ");
    }
        }
    public void mostrarArreglo(String [] Arreglo){
    for (String i : Arreglo) {
            System.out.print(i + ", ");
    
    }}
    public static int busq_binaria(String[] arr,String dato){
     //declaramos las variables para conocer el dato de la mitad del arreglo
     //que solo vamos a usar una vez
     int posicion = 0;
     int min = 0;
     int max = arr.length -1;
     int i = 0;
     boolean enc = false;
     double mitad = (double)(min+max)/2;
     mitad = Math.round(mitad);
     int media = (int)mitad;
     int mitad_estatica = media;   
     
     //creamos un bucle condicional while que nos servira para comparar los contenidos 
     //del arreglo 
     while(i<arr.length){ 
         //si entra a esta condicion quiere decir que se encontro el dato
         if(arr[media].equals(dato)){
             enc = true;
             posicion = media;
             break;          
        }
         if(media >= mitad_estatica){
	    media = media + 1;
	}
	if(media > arr.length-1){
	    media = -1;
	}  
	if(media <= mitad_estatica){
            media = media + 1;
        }
        
         i++;
         
     }
        
    //se utiliza la bandera declara para saber si se encontro o no el dato
     if (enc == true){

     }
     else{
         System.out.println("No se encontro el dato ingresado");
         posicion = -1;
     }
     //retornamos la variable posicion para imprimir los demas arreglos
     return posicion;
    }
     
    
    public static void impdat(String[] Ap, String [] Nom, String [] Tel, int posicion){
    
        System.out.println("Apellido:   "+Ap[posicion]);
        System.out.println("Telefono:   "+Nom[posicion]);
        System.out.println("Nombre:     "+Tel[posicion]);
        System.out.println("---------------------------------------");
    }
}
